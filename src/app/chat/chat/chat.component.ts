import { Component } from '@angular/core';
import { RxStompService } from 'src/app/services/rx-stomp.service';
import { Message } from '@stomp/stompjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent {
  constructor(private rxStompService: RxStompService) {}

  receivedMessages: string[] = [];


  ngOnInit(): void {
    this.rxStompService.watch('/queue/message/chat').subscribe((message: Message) => {
      console.log(message.body);
      this.receivedMessages.push(JSON.stringify(message.body));
    });
  }

  onSendMessage() {
    const message = `Message generated at ${new Date()}`;
    this.rxStompService.publish({ destination: '/topic/demo', body: message });
  }
}
