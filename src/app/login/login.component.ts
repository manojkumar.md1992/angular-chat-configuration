import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
    
    constructor(private http : HttpClient, private router : Router){}

  loginNow(){
      this.http.post('http://localhost:8200/login', {
        "username" : "9050928575",
        "password" : "3505",
        "instituteId" : "1"
    }).subscribe((response : any) => {
        const token = response.access_token;
        localStorage.setItem('token', token);
        this.router.navigateByUrl('/chat');
    });
  }

}
