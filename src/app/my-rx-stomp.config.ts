import { RxStompConfig } from '@stomp/rx-stomp';
import { RxStompService } from './services/rx-stomp.service';

export const myRxStompConfig: RxStompConfig = {
  // Which server?
  brokerURL: 'ws://127.0.0.1:8200/ws',

  // Headers
  // Typical keys: login, passcode, host
  connectHeaders: {
    Authorization : `Bearer eyJhbGciOiJIUzM4NCJ9.eyJ1c2VybmFtZSI6IjkwNTA5Mjg1NzUiLCJpYXQiOjE2NzY1NTA0OTgsImV4cCI6MTY3ODM3Nzg4Nn0.jVUeVr1ElRpXRxx3MgK1vi4gR2avwtOttKYGGGDRbsWrWrzEEUvyoNFkq2o5HBtq`
  },

  // How often to heartbeat?
  // Interval in milliseconds, set to 0 to disable
  heartbeatIncoming: 0, // Typical value 0 - disabled
  heartbeatOutgoing: 20000, // Typical value 20000 - every 20 seconds

  // Wait in milliseconds before attempting auto reconnect
  // Set to 0 to disable
  // Typical value 500 (500 milli seconds)
  reconnectDelay: 20000,

  // Will log diagnostics on console
  // It can be quite verbose, not recommended in production
  // Skip this key to stop logging to console
  debug: (msg: string): void => {
    console.log(new Date(), msg);
  },
};